import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionFinancieraCampanaComponent } from './informacion-financiera-campana.component';

describe('InformacionFinancieraCampanaComponent', () => {
  let component: InformacionFinancieraCampanaComponent;
  let fixture: ComponentFixture<InformacionFinancieraCampanaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InformacionFinancieraCampanaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(InformacionFinancieraCampanaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
