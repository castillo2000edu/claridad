import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionFinancieraAnualComponent } from './informacion-financiera-anual.component';

describe('InformacionFinancieraAnualComponent', () => {
  let component: InformacionFinancieraAnualComponent;
  let fixture: ComponentFixture<InformacionFinancieraAnualComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InformacionFinancieraAnualComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(InformacionFinancieraAnualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
