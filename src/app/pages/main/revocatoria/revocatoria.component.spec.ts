import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RevocatoriaComponent } from './revocatoria.component';

describe('RevocatoriaComponent', () => {
  let component: RevocatoriaComponent;
  let fixture: ComponentFixture<RevocatoriaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RevocatoriaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RevocatoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
