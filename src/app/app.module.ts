import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './pages/shared/header/header.component';
import { MainComponent } from './pages/main/main.component';
import { InformacionFinancieraCampanaComponent } from './pages/main/informacion-financiera-campana/informacion-financiera-campana.component';
import { Pantalla1Component } from './pages/main/pantalla1/pantalla1.component';
import { InformacionFinancieraAnualComponent } from './pages/main/informacion-financiera-anual/informacion-financiera-anual.component';
import { RevocatoriaComponent } from './pages/main/revocatoria/revocatoria.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    InformacionFinancieraCampanaComponent,
    Pantalla1Component,
    InformacionFinancieraAnualComponent,
    RevocatoriaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
